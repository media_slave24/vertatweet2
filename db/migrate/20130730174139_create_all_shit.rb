class CreateAllShit < ActiveRecord::Migration
  def change
    create_table :tweets do |t|
      t.belongs_to :point
      t.datetime :tweeted_at
      t.string :tweet_id
      t.string :author
      t.boolean :retweet

      t.timestamps
    end

    create_table :keywords do |t|
      t.string :name
      t.belongs_to :map

      t.timestamps
    end

    create_table :tweets_keywords do |t|
      t.belongs_to :keyword
      t.belongs_to :tweet
    end

    create_table :points do |t|
      t.belongs_to :map
      t.decimal :lat, precision: 7, scale: 4
      t.decimal :lng, precision: 7, scale: 4
      t.integer :radius

      t.timestamps
    end

    create_table :maps do |t|
      t.string :name, null: false
      t.string :country, null: false

      t.timestamps
    end
  end
end

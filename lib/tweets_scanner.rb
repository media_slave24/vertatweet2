# Takes keyword @String and point @Point.
# point responds to .lat, .lng, .radius (km)
class TweetsScanner
  def self.scan(keyword, points)
    self.new(keyword, points).scan.results
  end

  attr_reader :tweets
  def initialize(keyword, points)
    @keyword = keyword
    @points = points
    @app_key = "236854469-2muGelEyTQfZ50mxvN76Rzr3fTNHV6hmfyOrrkxW"
    @app_secret = "WJvIJDlL1iDdtUXclmAzA4qDbfsuzHSHSEKyOMssqY"
    @oauth_token = "dvkRUASjzhfE04IaOhgjYg" 
    @oauth_token_secret = "NHPOINOot5PjBIw5BxEDUJC0rvuP2zooXs3kLwTo"

    @max_id = {}
    @results = {}
  end

  def scan
    @points.each do |point|
      scan_point(point)
    end
    self
  end
  
  def scan_point(point)
    loop do
      response =  search(point)
      
      results = response.results

      break if results.count < 100
    end
  end

  def search(*args)
    twitter_client.search @keyword, client_settings(*args)
  end

  def save_tweet(point, status)
    @tweets[point] ||= []
    @tweets[point] << status
  end

  def save_tweets(point, statuses)
    statuses.each do |status|
      save_tweet(point, status)
    end
  end

  def client_settings(point)
    default_client_settings.
      merge({
        geocode: "#{point.lat.to_s},#{point.lng.to_s},#{point.radius.to_s}km",
      }).
      reject{|k,v|v.nil?}
  end

  def default_client_settings
    {
      result_type: "recent",
      count: 100,
      include_entities: true
    }
  end

  private
    def twitter_client
      @twitter_client ||= Twitter::Client.new(
          :consumer_key => @app_key,
          :consumer_secret => @app_secret,
          :oauth_token => @oauth_token,
          :oauth_token_secret => @oauth_token_secret
        )
    end
end

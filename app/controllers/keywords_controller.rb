class KeywordsController < ActionController::Base
  def create
    keyword = Keyword.create(keyword_params)
    respond_to do |f|
      f.json do
        render json: keyword.to_json
      end
    end
  end

  def scan
    Keyword.find(params[:id])
    Scanner.scan(Keyword.name, Keyword.map.points.to_a)
  end

  private
    def keyword_params
      params.
        require(:keyword).
        permit(:name, :map_id)
    end
end

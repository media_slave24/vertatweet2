// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require knockout

(function ($) {
  'use strict';
  /*global google: false */
  var setCountryName = function (latLng, el) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({latLng: latLng }, function (results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        el.val(results[results.length - 1].formatted_address);
      } else { window.alert('Geocoder failed due to: ' + status); }
    });
  },

    gmapInit = function () {
      var mapOptions = {
        center: new google.maps.LatLng(49.840197, 24.121078),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      },

        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions),

        countryMarker = new google.maps.Marker({
          position: new google.maps.LatLng(49.840197, 24.02813),
          title: 'Country',
          map: map,
          icon: '/country.png',
          draggable: true
        }),

        bottomRightMarker = new google.maps.Marker({
          position: new google.maps.LatLng(49.840197, 24.02813),
          title: 'bottomRight',
          map: map,
          icon: '/bottom_right.png',
          draggable: true
        }),

        topLeftMarker = new google.maps.Marker({
          position: new google.maps.LatLng(49.840197, 24.02813),
          title: 'topLeft',
          map: map,
          icon: '/top_left.png',
          draggable: true
        }),
        topLeft = $(".top-left"),
        bottomRight = $(".bottom-right"),
        country = $(".country"),
        countryName = $(".country-name");

      google.maps.event.addListener(topLeftMarker, 'dragend', function (e) {
        var val = e.latLng.lat().toFixed(5) + "," + e.latLng.lng().toFixed(5);
        topLeft.val(val);
      });
      google.maps.event.addListener(bottomRightMarker, 'dragend', function (e) {
        var val = e.latLng.lat().toFixed(5) + "," + e.latLng.lng().toFixed(5);
        bottomRight.val(val);
      });
      google.maps.event.addListener(countryMarker, 'dragend', function (e) {
        var val = e.latLng.lat().toFixed(5) + "," + e.latLng.lng().toFixed(5);
        country.text(val);
        setCountryName(e.latLng, countryName);
      });

      window.map = map;
      window.countryMarker = countryMarker;
      window.bottomRightMarker = bottomRightMarker;
      window.topLeftMarker = topLeftMarker;
    },

    addMarker = function (lat, lng) {
      return new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: window.map
      });
    },

    renderPoints = function (points) {
      $.each(points, function (i, point) {
        window.markers = window.markers || [];
        window.markers[window.markers.length] = addMarker(point.lat, point.lng);
      });
    },

    clearMarkers = function () {
      window.markers = window.markers || [];
      if (window.markers.length === 0) { return; }
      $.each(window.markers, function (i, m) {
        m.setMap(null);
      });
    },

    renderGrid = function (grid) {
      $.each(grid, function (i, line) {
        renderPoints(line);
      });
    };

  $("#griddable-form").on("ajax:success", function (event, data) {
    clearMarkers();
    renderGrid(data);
  });

  $("#show-map").on("ajax:success", function (event, data) {
    clearMarkers();
    renderPoints(data);
  });

  $("#griddable-form, #show-map").on("ajax:error", function (xhr, status) {
    window.alert("There was error with griddable, server returned status: " + status.code);
  });

  $('#tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });

  google.maps.event.addDomListener(window, 'load', gmapInit);
}(jQuery));

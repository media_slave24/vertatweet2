# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130730174139) do

  create_table "keywords", force: true do |t|
    t.string   "name"
    t.integer  "map_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "maps", force: true do |t|
    t.string   "name",       null: false
    t.string   "country",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "points", force: true do |t|
    t.integer  "map_id"
    t.decimal  "lat",        precision: 7, scale: 4
    t.decimal  "lng",        precision: 7, scale: 4
    t.integer  "radius"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tweets", force: true do |t|
    t.integer  "point_id"
    t.datetime "tweeted_at"
    t.string   "tweet_id"
    t.string   "author"
    t.boolean  "retweet"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tweets_keywords", force: true do |t|
    t.integer "keyword_id"
    t.integer "tweet_id"
  end

end

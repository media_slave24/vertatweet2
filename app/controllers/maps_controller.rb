class MapsController < ActionController::Base
  def show_one
    map = Map.find(params[:id])
    respond_to do |f|
      f.json do
        render json: map.points.to_a.to_json
      end
    end
  end

  def create
    map = Map.create(map_params)
    griddable = Geocoder::Griddable.new(geocoder_params)
    grid = griddable.to_grid(radius_params)
    grid.lines.each do |line|
      line.each do |point|
        Point.create(map: map,
                     lat: point.lat,
                     lng: point.lng,
                     radius: point.km_radius)
      end
    end
    respond_to do |f|
      f.json do
        render json: grid.lines
      end
    end
  end

  private
    def geocoder_params
      params.
        require(:griddable).
        permit(%i{ top_left bottom_right name }).
        merge(rule: "country")
    end

    def radius_params
      params.
        require(:griddable).
        permit(:radius)["radius"].to_i
    end

    def map_params
      params.
        require(:griddable).
        permit(:name)
    end
end

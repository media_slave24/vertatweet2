class Map < ActiveRecord::Base
  has_many :points

  before_validation do |record|
    record.country = record.name
  end
end
